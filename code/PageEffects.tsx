import * as React from "react"
import { useState } from "react"
import { Frame, Page, Stack, Data, transform } from "framer"

const pages = ["One", "Two", "Three", "Four", "Five"]

export function PageEffects() {
    const [pageNumber, setPageNumber] = useState(0)

    return (
        <Frame size={"100%"} background={"none"}>
            <Stack size={"100%"}>
                <Page
                    width={"100%"}
                    height={"100%"}
                    overflow={"visible"}
                    gap={10}
                    effect={info => {
                        const offset = info.normalizedOffset
                        const opacity = transform(offset, [-1, 0, 1], [0, 1, 0])
                        const scale = transform(
                            offset,
                            [-1, 0, 1],
                            [0.6, 1, 0.6]
                        )

                        return { opacity, scale }
                    }}
                    onChangePage={currentPage => {
                        setPageNumber(currentPage)
                    }}
                    currentPage={pageNumber}
                >
                    {pages.map((title, index) => {
                        return (
                            <Frame
                                size={"100%"}
                                background={"#fff"}
                                radius={30}
                                key={index}
                            >
                                {title}
                            </Frame>
                        )
                    })}
                </Page>
                <Stack
                    width={"100%"}
                    height={"10px"}
                    background={"none"}
                    direction={"horizontal"}
                    distribution={"center"}
                >
                    {pages.map((title, index) => {
                        return (
                            <Frame
                                size={"10px"}
                                borderRadius={50}
                                key={index}
                                background={"#fff"}
                                animate={{
                                    opacity: index === pageNumber ? 1 : 0.3,
                                    scale: index === pageNumber ? 1 : 0.7,
                                }}
                                onTap={() => {
                                    setPageNumber(index)
                                }}
                                style={{ marginTop: "30px" }}
                            />
                        )
                    })}
                </Stack>
            </Stack>
        </Frame>
    )
}
